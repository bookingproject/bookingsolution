
# Booking Solution

Root project for the Room Booking solution. This project is usefull to keep a working snapshot of the solution splitted into three different projects.

* A Smart contract
* A Spring Boot server
* A React web application

A root project is also usefull to deploy all the solution using a single docker command. **The docker-compose file is here as an example but I deployed the two services separately using their docker image.**

## Project Overview

The solution has been designed as follow :

```mermaid
graph LR
A[COKE Web application] --- B[COKE Booking Service]
C[PEPSI Web application] --- D[PEPSI Booking Service]
B --- E{Booking Contract}
D --- E
```

There are two instances of the Booking Service and the Web application deployed with company specifics parameters such as the WALLET_PRIVATE_KEY.

For this demonstration, I deployed the Coke Web application and the Coke Booking service on Google Cloud and the Booking Contract on Ethereum-Rinkeby.

Feel free to check the three subprojects, I am looking forward to presenting the project and answering any of your question during the interview :)
